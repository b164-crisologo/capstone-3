import {Fragment, useEffect, useState} from 'react'
import CourseCard from '../components/CourseCard';
//import coursesData from '../data/coursesData';

export default function Products() {

	const [products, setProducts] = useState([])
	//console.log(coursesData[0])

	// const courses = coursesData.map(course => {

	// 	return(
	// 		<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })

	useEffect(() => {
		fetch("https://gentle-stream-83686.herokuapp.com/api/product/allActive")
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(products => {
				return(
					<CourseCard key={products._id} courseProp={products} />
					)
			}))
		})	
	}, [])

	return(
		<Fragment>
			<h1>Products</h1>
			{products}
		</Fragment>

		)
}
