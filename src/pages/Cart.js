import { Row, Col, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import React from 'react';
import CourseCard from '../components/CourseCard';

export default function CartPage() {


	return(

		<Row>
			<Col className="p-5">
			<div>
				<h1>Cart Page</h1>
			</div>
				<p>The page you are looking cannot be found</p>
				<Button variant="primary" as={Link } to="/" >Back Home</Button>
			</Col>
		</Row>

		)

}