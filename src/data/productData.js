const productData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe sequi error nostrum molestiae aliquam eaque quod nam vel tenetur, voluptatibus impedit aliquid, architecto eos ea? Eligendi quaerat aut velit ut?",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe sequi error nostrum molestiae aliquam eaque quod nam vel tenetur, voluptatibus impedit aliquid, architecto eos ea? Eligendi quaerat aut velit ut?",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe sequi error nostrum molestiae aliquam eaque quod nam vel tenetur, voluptatibus impedit aliquid, architecto eos ea? Eligendi quaerat aut velit ut?",
		price: 55000,
		onOffer: true
	}

]

export default coursesData;