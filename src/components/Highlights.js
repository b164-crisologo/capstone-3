import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {


	return(

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>All Brand New</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium assumenda ad enim a officiis in, pariatur accusamus recusandae adipisci, tempora perspiciatis explicabo, ratione aliquam expedita ipsa similique voluptates harum suscipit.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>Complete with Box and Original Receipts</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium assumenda ad enim a officiis in, pariatur accusamus recusandae adipisci, tempora perspiciatis explicabo, ratione aliquam expedita ipsa similique voluptates harum suscipit.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>Easy Transactions</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium assumenda ad enim a officiis in, pariatur accusamus recusandae adipisci, tempora perspiciatis explicabo, ratione aliquam expedita ipsa similique voluptates harum suscipit.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

		)
}