import {Row, Col, Button, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {

	return(
		<Container className="p-5">
		<Row className="justify-content-center">
			<Col className="p-5 text-center">
				<h1>BNDS</h1>
				<p>Brand New Dead Stock</p>
				<Button variant="primary" as={Link} to={`/product/`}>Let's have a deal</Button>
			</Col>
		</Row>
		</Container>
		)

}